<?php
/**
 * Based on ideas from Stanford's Webauth modules, this one was
 * developed for University College Oxford by Olamalu.
 */


function webauth_menu() {
  $items['admin/config/system/webauth'] = array(
    'title' => 'Webauth',
    'description' => 'Configure Weabauth',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webauth_admin_settings_form'),
    'access arguments' => array('administer webauth'),
    'file' => 'webauth.admin.inc',
  );
  $items['webauth/authenticate'] = array(
      'title' => 'Webauth Login',
      'page callback' => 'webauth_authentication_page',
      'access callback' => 'user_is_anonymous',
      'type' => MENU_CALLBACK,
      'file' => 'webauth.pages.inc',
  );
  return $items;
}

/**
 * Implements hook_help().
 */
function webauth_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/modules#description':
      $output .= t('Webauth authentication module.');
      break;
    case 'admin/config/system/webauth':
      $output .= t('<p>Information about Drupal webauth can be found at !link</p>', array('!link' => l('admin/structure/block', 'admin/structure/block')));
      break;
  }
  return $output;
}

/**
 * Implements hook_init().
 */
function webauth_init() {
  
}

/**
 * Implements hook_permission().
 */
function webauth_permission() {
  return array(
      'administer webauth' => array(
          'title' => t('Admin webauth authentication'),
          'description' => t('Admin the webauth authentication module settings page.'),
      ),
  );
}

function webauth_theme() {
  return array(
      // Theme function for the 'sortable' example
      'webauth_login_link' => array(
          'render element' => 'form',
      ),
  );
}

/**
 * Override the login form
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $form_id
 */
function webauth_form_alter(&$form, $form_state, $form_id) {
  
  global $user, $conf;

  if ($form_id == 'user_login_block' || $form_id == 'user_login') {

    if (isset($form_state['weblogin']['server'])) {
      $form['#validate'] = array('webauth_auth');
    }
    drupal_session_start();

    // build a link to wa_login.
    // added double quotes to webauth link - Lance Simms 8/13/08
    $wa_url = '<a href="/files/webauth/login.php">'.variable_get('webauth_link_text','Webauth Login').'</a>';
    $form['webauth_link'] = array(
        '#prefix' => '<div id="webauth-link">',  // make it themable
        '#markup' => $wa_url,
        '#suffix' => '</div>',
        '#weight' => -10
    );
    //$form['name']['#required'] = FALSE;
    //$form['pass']['#required'] = FALSE;
    //unset($form['#submit']);
    //$form['#validate'] = array('webauth_login_validate');
    
    $allow_local = variable_get('webauth_allow_local', TRUE);
    if (!$allow_local) {
      unset($form['name']);
      unset($form['pass']);
      unset($form['links']);
    }
  }
  $form['webauth.return_to'] = array('#type' => 'hidden', '#value' => url('user', array('absolute' => TRUE, 'query' => user_login_destination())));
}

/**
 * Login form _validate hook
 */
function webauth_login_validate($form, &$form_state) {
  $return_to = $form_state['values']['webauth.return_to'];
  if (empty($return_to)) {
    $return_to = url('', array('absolute' => TRUE));
  }

  webauth_begin($return_to, $form_state['values']);
}

function webauth_begin($return_to, $form_state) {
  drupal_goto('files/webauth/login.php');
  //TODO Get the destinations sorted
}

function webauth_complete() {
  $result = array();
  if ($_SESSION['wa_data'] && $_SESSION['wa_data']['wa_user']) {
    $result['status'] = 'success';
  } else {
    $result['status'] = 'failed';
  }
  
  return $result;
}

function webauth_authentication() {
  
  $identity = $_SESSION['wa_data']['wa_user'];
  //TODO This is a klutch for testing - need to integrate to authmap
  //Drupal 7 doesn't allow certain webauth punctuation as usernames.
  $identity = preg_replace('/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i', '', $identity);
  $account = user_load_by_name($identity); //user_load_external is what we want here
  
  if (isset($account->uid)) {
    if (!variable_get('user_email_verification', FALSE) || $account->login) {
      // Check if user is blocked.
      $state['values']['name'] = $account->name;
      user_login_name_validate(array(), $state);
      if (!form_get_errors()) {
        // Load global $user and perform final login tasks.
        $form_state['uid'] = $account->uid;
        user_login_submit(array(), $form_state);
      }
    }
    else {
      drupal_set_message(t('You must validate your email address for this account before logging in via OpenID.'));
    }
  } else {
    drupal_set_message(t('You are not yet set up as a user on this site.'), 'error');
  }
  //TODO Go back to OpenID to check other cases of authentication are covered
  drupal_goto();
}